# MMA::LASERZZ

*Jiří Doležal, Vadim Petrov*

- [openFrameworks](http://openframeworks.cc/): An open source C++ toolkit for creative coding
- [ofxIlda](https://github.com/memo/ofxIlda): C++ openFrameworks addon for a device agnostic Ilda functionality
- [latest ILDA format spec](./doc/ILDA_IDTF14_rev011.pdf)