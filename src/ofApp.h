#pragma once

#include "ofMain.h"
#include "ofxIlda.h"
//#include "ofxIldaFrame.h"
//#include "ofxIldaPoint.h"
//#include "ofxIldaPoly.h"
//#include "ofxIldaPolyProcessor.h"
//#include "ofxIldaRenderTarget.h"

#include "StclDevices.h"

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);



	ofxIlda::Frame ildaFrame;   // stores and manages ILDA frame drawings

};
